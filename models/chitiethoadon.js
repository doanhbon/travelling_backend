const { Model } = require('sequelize');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('../utils/custom_error');

module.exports = (sequelize, DataTypes) => {
  class ChiTietHoaDon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
      ChiTietHoaDon.belongsTo(models.HoaDon, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'hoadon_id',
        },
      });
      ChiTietHoaDon.belongsTo(models.SanPham, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'sanpham_id',
        },
      });
    }
  }
  ChiTietHoaDon.init(
    {
      gia_mua: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      gia_ban: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      so_luong: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value < 0) {
              throw new CustomError('DATA_011', CUSTOM_ERROR_CODE.DATA_011);
            }
          },
        },
      },
    },
    {
      sequelize,
      modelName: 'ChiTietHoaDon',
    }
  );
  return ChiTietHoaDon;
};
