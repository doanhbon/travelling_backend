const { Model } = require('sequelize');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('../utils/custom_error');

module.exports = (sequelize, DataTypes) => {
  class Tour extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
      Tour.hasMany(models.KeHoachTour, { foreignKey: 'tour_id' });
      Tour.hasMany(models.TourKhoiHanh, { foreignKey: 'tour_id' });
      Tour.belongsToMany(models.SanPham, { through: 'TourSanPham' });
      Tour.hasMany(models.Feedback, { foreignKey: 'tour_id' });
      Tour.hasMany(models.Comment, { foreignKey: 'tour_id' });
    }
  }
  Tour.init(
    {
      ten_tour: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      mo_ta: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      gia_ve_cho_tre_em: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      gia_ve_cho_nguoi_lon: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      so_ngay_di: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_015', CUSTOM_ERROR_CODE.DATA_015);
            }
          },
        },
      },
      giam_gia: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        defaultValue: 0,
        validate: {
          wrongPriceFormat(value) {
            if (value < 0 || value > 1) {
              throw new CustomError('DATA_014', CUSTOM_ERROR_CODE.DATA_014);
            }
          },
        },
      },
      image_url: {
        type: DataTypes.STRING,
        allowNull: true,
        validate: {
          wrongUrlFormat(value) {
            // eslint-disable-next-line no-useless-escape
            const re =
              /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm;
            if (!re.test(value)) {
              throw new CustomError('DATA_008', CUSTOM_ERROR_CODE.DATA_008);
            }
          },
        },
      },
      so_luot_danh_gia: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        defaultValue: 0,
      },
      diem_danh_gia_trung_binh: {
        type: DataTypes.FLOAT,
        allowNull: false,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      modelName: 'Tour',
    }
  );
  return Tour;
};
