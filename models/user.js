const { Model } = require('sequelize');
const CustomError = require('../utils/custom_error');

const CONSTANTS = require('../constants');

const { CUSTOM_ERROR_CODE } = CONSTANTS;

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.UserAuthentication, { foreignKey: 'user_id' });
      User.hasMany(models.HoaDon, { foreignKey: 'user_id' });
      User.hasMany(models.Feedback, { foreignKey: 'user_id' });
      User.hasMany(models.Comment, { foreignKey: 'user_id' });
    }
  }
  User.init(
    {
      ho: {
        type: DataTypes.STRING,
      },
      ten: {
        type: DataTypes.STRING,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          wrongEmailFormat(value) {
            // eslint-disable-next-line prettier/prettier
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value.toLowerCase())) {
              throw new CustomError('DATA_001', CUSTOM_ERROR_CODE.DATA_001);
            }
          },
        },
      },
      gioi_tinh: { type: DataTypes.BOOLEAN },
      so_dien_thoai: {
        type: DataTypes.STRING,
        unique: true,
        validate: {
          wrongPhoneNumberFormat(value) {
            const re = /^[0-9]{10}$/i;
            if (value !== null && !re.test(value)) {
              throw new CustomError('DATA_002', CUSTOM_ERROR_CODE.DATA_002);
            }
          },
        },
      },
      ngay_sinh: { type: DataTypes.DATEONLY },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          wrongUserNameFormat(value) {
            // eslint-disable-next-line prettier/prettier
            const re = /^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/i;
            if (value && !re.test(value)) {
              throw new CustomError('DATA_004', CUSTOM_ERROR_CODE.DATA_004);
            }
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          wrongPasswordFormat(value) {
            const re = /^[0-9a-fA-Z]{64}$/i;
            if (!re.test(value)) {
              throw new CustomError('DATA_003', CUSTOM_ERROR_CODE.DATA_003);
            }
          },
        },
      },
      is_verified: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      is_admin: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      is_employee: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      avatar_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'User',
    }
  );
  return User;
};
