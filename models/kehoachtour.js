const { Model } = require('sequelize');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('../utils/custom_error');

module.exports = (sequelize, DataTypes) => {
  class KeHoachTour extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      KeHoachTour.belongsTo(models.Tour, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'tour_id',
        },
      });
      KeHoachTour.belongsTo(models.DiaDiem, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: true,
          name: 'dia_diem_id',
        },
      });
    }
  }
  KeHoachTour.init(
    {
      mo_ta: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      thu_tu_ngay: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: 'compositeIndexThutu',
        validate: {
          wrongThuTuFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_016', CUSTOM_ERROR_CODE.DATA_016);
            }
          },
        },
      },
      index: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: 'compositeIndexThutu',
        validate: {
          wrongIndexFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_017', CUSTOM_ERROR_CODE.DATA_017);
            }
          },
        },
      },
    },
    {
      sequelize,
      modelName: 'KeHoachTour',
    }
  );
  return KeHoachTour;
};
