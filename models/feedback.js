const { Model } = require('sequelize');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('../utils/custom_error');

module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Feedback.belongsTo(models.User, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'user_id',
        },
      });
      Feedback.belongsTo(models.Tour, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'tour_id',
        },
      });
    }
  }
  Feedback.init(
    {
      score: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        defaultValue: 0,
        validate: {
          wrongIndexFormat(value) {
            if (value < 0 || value > 5) {
              throw new CustomError('DATA_020', CUSTOM_ERROR_CODE.DATA_020);
            }
          },
        },
      },
      is_valid: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      mota: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'Feedback',
    }
  );
  return Feedback;
};
