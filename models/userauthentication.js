const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class UserAuthentication extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
      UserAuthentication.belongsTo(models.User, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'user_id',
        },
      });
    }
  }
  UserAuthentication.init(
    {
      confirm_email_token: DataTypes.STRING,
      reset_password_token: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'UserAuthentication',
    }
  );
  return UserAuthentication;
};
