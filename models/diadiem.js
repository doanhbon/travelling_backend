const { Model } = require('sequelize');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('../utils/custom_error');

module.exports = (sequelize, DataTypes) => {
  class DiaDiem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      DiaDiem.hasMany(models.KeHoachTour, { foreignKey: 'dia_diem_id' });
    }
  }
  DiaDiem.init(
    {
      ten: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      dia_chi: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      mo_ta: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      chi_phi_goc: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      gia_khi_qua_dia_diem: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      image_url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'DiaDiem',
    }
  );
  return DiaDiem;
};
