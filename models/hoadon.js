const { Model } = require('sequelize');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('../utils/custom_error');

module.exports = (sequelize, DataTypes) => {
  class HoaDon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      HoaDon.belongsTo(models.User, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'user_id',
        },
      });
      HoaDon.belongsTo(models.TourKhoiHanh, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'tour_khoi_hanh_id',
        },
      });
      HoaDon.hasMany(models.ChiTietHoaDon, { foreignKey: 'hoadon_id' });
    }
  }
  HoaDon.init(
    {
      ho_ten: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      so_dien_thoai: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          wrongPhoneNumberFormat(value) {
            const re = /^[0-9]{10}$/i;
            if (value !== null && !re.test(value)) {
              throw new CustomError('DATA_002', CUSTOM_ERROR_CODE.DATA_002);
            }
          },
        },
      },
      dia_chi: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      so_cmnd: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          wrongDescriptionFormat(value) {
            // eslint-disable-next-line prettier/prettier
            const re = /^[0-9]{9,12}$/i;
            if (!re.test(value.toLowerCase())) {
              throw new CustomError('DATA_012', CUSTOM_ERROR_CODE.DATA_012);
            }
          },
        },
      },
      ngay_sinh: { type: DataTypes.DATEONLY, allowNull: false },
      so_nguoi_lon: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value < 0) {
              throw new CustomError('DATA_011', CUSTOM_ERROR_CODE.DATA_011);
            }
          },
        },
      },
      so_tre_em: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value < 0) {
              throw new CustomError('DATA_011', CUSTOM_ERROR_CODE.DATA_011);
            }
          },
        },
      },
      gia_ve_nguoi_lon: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value < 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      gia_ve_tre_em: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        validate: {
          wrongPriceFormat(value) {
            if (value < 0) {
              throw new CustomError('DATA_010', CUSTOM_ERROR_CODE.DATA_010);
            }
          },
        },
      },
      da_thanh_toan: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ghiChu: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      sequelize,
      modelName: 'HoaDon',
    }
  );
  return HoaDon;
};
