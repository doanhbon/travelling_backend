const { Model } = require('sequelize');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('../utils/custom_error');

module.exports = (sequelize, DataTypes) => {
  class TourKhoiHanh extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      TourKhoiHanh.belongsTo(models.Tour, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'tour_id',
        },
      });
      TourKhoiHanh.hasMany(models.HoaDon, { foreignKey: 'tour_khoi_hanh_id' });
    }
  }
  TourKhoiHanh.init(
    {
      so_ve: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          wrongCountFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_011', CUSTOM_ERROR_CODE.DATA_011);
            }
          },
        },
      },
      so_ve_con_lai: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          wrongCountFormat(value) {
            if (value <= 0) {
              throw new CustomError('DATA_011', CUSTOM_ERROR_CODE.DATA_011);
            }
          },
        },
      },
      ngay_xuat_phat: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
          wrongDatePoint(value) {
            if (value.getTime() < Date.now()) {
              throw new CustomError('DATA_018', CUSTOM_ERROR_CODE.DATA_018);
            }
          },
        },
      },
      mo_ta: {
        allowNull: true,
        type: DataTypes.STRING,
      },
    },
    {
      sequelize,
      modelName: 'TourKhoiHanh',
    }
  );
  return TourKhoiHanh;
};
