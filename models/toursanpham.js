const { Model } = require('sequelize');

// eslint-disable-next-line no-unused-vars
module.exports = (sequelize, DataTypes) => {
  class TourSanPham extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
    }
  }
  TourSanPham.init(
    {},
    {
      sequelize,
      modelName: 'TourSanPham',
      tableName: 'TourSanPham',
    }
  );
  return TourSanPham;
};
