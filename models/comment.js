const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Comment.belongsTo(models.User, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'user_id',
        },
      });
      Comment.belongsTo(models.Tour, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: false,
          name: 'tour_id',
        },
      });
      Comment.hasMany(Comment, {
        onDelete: 'CASCADE',
        foreignKey: {
          allowNull: true,
          name: 'parent_id',
        },
      });
    }
  }
  Comment.init(
    {
      noi_dung: {
        type: DataTypes.STRING(1000),
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Comment',
    }
  );
  return Comment;
};
