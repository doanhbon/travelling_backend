const jwt = require('jsonwebtoken');

const { customResponseData } = require('./utils/common');
const CustomError = require('./utils/custom_error');

const CONSTANTS = require('./constants');

const { CUSTOM_ERROR_CODE, SECRET_KEY } = CONSTANTS;

const customResponseFilter = (request, response, next) => {
  try {
    const oldJSON = response.json;
    response.json = (data) => {
      // For Async call, handle the promise and then set the data to `oldJson`
      if (data && data.then !== undefined) {
        // Resetting json to original to avoid cyclic call.
        return data
          .then((responseData) => {
            const formattedResponseData = customResponseData(responseData);
            response.json = oldJSON;
            return oldJSON.call(response, formattedResponseData);
          })
          .catch((error) => {
            next(error);
          });
      }

      const formattedResponseData = customResponseData(data);
      response.json = oldJSON;
      return oldJSON.call(response, formattedResponseData);
    };

    next();
  } catch (error) {
    next(error);
  }
};

// eslint-disable-next-line no-unused-vars
const handleError = (err, req, res, next) => {
  console.log(err.stack);
  res.json(err);
};

const authenticationMiddleware = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    throw new CustomError('AUTH_001', CUSTOM_ERROR_CODE.AUTH_001);
  }

  const token = authorization.split('Bearer ')[1];
  jwt.verify(token, SECRET_KEY, (err, decoded) => {
    if (err) {
      throw new CustomError('AUTH_002', CUSTOM_ERROR_CODE.AUTH_002);
    }

    req.user = {
      ...decoded,
    };

    next();
  });
};

const checkSameUser = (req, res, next) => {
  const { user } = req;
  if (!user) {
    throw new CustomError('AUTH_001', CUSTOM_ERROR_CODE.AUTH_001);
  }
  // eslint-disable-next-line camelcase
  const { username } = req.params;
  // eslint-disable-next-line camelcase
  if (user.username !== username) {
    throw new CustomError('AUTH_003', CUSTOM_ERROR_CODE.AUTH_003);
  }

  next();
};

const checkEmployee = (req, res, next) => {
  const { user } = req;
  if (!user.is_employee) {
    throw new CustomError('ADMIN_001', CUSTOM_ERROR_CODE.ADMIN_001);
  }
  next();
};

const checkAdmin = (req, res, next) => {
  const { user } = req;
  if (!user.is_admin) {
    throw new CustomError('ADMIN_001', CUSTOM_ERROR_CODE.ADMIN_001);
  }
  next();
};

module.exports = {
  customResponseFilter,
  handleError,
  authenticationMiddleware,
  checkSameUser,
  checkEmployee,
  checkAdmin,
};
