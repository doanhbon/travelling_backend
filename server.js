require('dotenv').config({ path: `${__dirname}/.env` });
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const { customResponseFilter, handleError } = require('./middlewares');

const app = express();
app.use(
  '/api-docs',
  swaggerUi.serve,
  swaggerUi.setup(YAML.load('./swagger.yaml'))
);
app.use(express.json());
app.use(customResponseFilter);
app.use('/api/authentication', require('./routes/authentication'));
app.use('/api/users', require('./routes/user'));
app.use('/api/products', require('./routes/product'));
app.use('/api/places', require('./routes/place'));
app.use('/api/tours', require('./routes/tour'));
app.use('/api/tours', require('./routes/plant'));
app.use('/api/tours', require('./routes/trip'));
app.use('/api/tours', require('./routes/feedback'));
app.use('/api/tours', require('./routes/comment'));
app.use('/api/admins', require('./routes/admin'));
app.use('/api/employees', require('./routes/employee'));
app.use('/api/bills', require('./routes/bill'));
app.use('/api/feedbacks', require('./routes/feedback_admin'));

app.get('/', (req, res) => res.send('Server is running!!!...'));

app.use(handleError);

app.listen(3000, () => {
  // eslint-disable-next-line no-console
  console.log('server is running on port 3000');
});
