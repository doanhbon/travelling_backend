const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware } = require('../middlewares');
const {
  createOrUpdateFeedback,
  getFeedbacks,
  deleteFeedback,
} = require('../services/feedback');

const router = express.Router();

router.post(
  '/:tour_id/feedbacks/',
  authenticationMiddleware,
  asyncHandler(async (req, res) => {
    const { id } = req.user;
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await createOrUpdateFeedback(req.body, tour_id, id));
  })
);

router.get('/:tour_id/feedbacks/', async (req, res) => {
  // eslint-disable-next-line camelcase
  const { tour_id } = req.params;
  const { limit = 10, offset = 0 } = req.query;
  return res.json(
    await getFeedbacks(tour_id, parseInt(limit, 10), parseInt(offset, 10))
  );
});

router.delete(
  '/:tour_id/feedbacks/:feedback_id',
  authenticationMiddleware,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id, feedback_id } = req.params;
    const { id } = req.user;
    return res.json(
      await deleteFeedback(parseInt(tour_id, 10), parseInt(feedback_id, 10), id)
    );
  })
);

module.exports = router;
