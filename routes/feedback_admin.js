const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkEmployee } = require('../middlewares');
const {
  getFeedbacks,
  deleteFeedback,
  changeFeedbackStatus,
} = require('../services/feedback_admin');

const router = express.Router();

router.get('/', authenticationMiddleware, checkEmployee, async (req, res) => {
  // eslint-disable-next-line camelcase
  const { limit = 10, offset = 0 } = req.query;
  if (req.query.is_valid === 'true') {
    req.query.is_valid = true;
  } else if (req.query.is_valid === 'false') {
    req.query.is_valid = false;
  }
  delete req.query.limit;
  delete req.query.offset;
  return res.json(
    await getFeedbacks(parseInt(limit, 10), parseInt(offset, 10), req.query)
  );
});

router.delete(
  '/:feedback_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { feedback_id } = req.params;
    return res.json(await deleteFeedback(parseInt(feedback_id, 10)));
  })
);

router.patch(
  '/:feedback_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { feedback_id } = req.params;
    // eslint-disable-next-line camelcase
    const { is_valid } = req.body;
    return res.json(
      await changeFeedbackStatus(parseInt(feedback_id, 10), is_valid)
    );
  })
);

module.exports = router;
