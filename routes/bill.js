const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkEmployee } = require('../middlewares');
const { getAllBills, changeBillStatus } = require('../services/bill');

const router = express.Router();

router.get(
  '/',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => res.json(await getAllBills()))
);

router.patch(
  '/:bill_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { bill_id } = req.params;
    // eslint-disable-next-line camelcase
    const { da_thanh_toan } = req.body;
    return res.json(
      await changeBillStatus(parseInt(bill_id, 10), da_thanh_toan)
    );
  })
);

module.exports = router;
