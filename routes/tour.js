const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkEmployee } = require('../middlewares');
const {
  createTour,
  getAllTours,
  deleteTour,
  updateTour,
  getTour,
  getSuggestedProducts,
  addSuggestedProduct,
  deleteSuggestedProduct,
} = require('../services/tour');
const { uploadImg } = require('../utils/upload');

const router = express.Router();

router.post(
  '/',
  authenticationMiddleware,
  checkEmployee,
  uploadImg.single('image'),
  // eslint-disable-next-line prettier/prettier
  asyncHandler(async (req, res) => res.json(await createTour(req.body, req.file)))
);

router.get('/', async (req, res) =>
  res.json(await getAllTours({ ...req.query }))
);

router.delete(
  '/:tour_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await deleteTour(parseInt(tour_id, 10)));
  })
);

router.patch(
  '/:tour_id',
  authenticationMiddleware,
  checkEmployee,
  uploadImg.single('image'),
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await updateTour(tour_id, req.body, req.file));
  })
);

router.get(
  '/:tour_id',
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await getTour(parseInt(tour_id, 10)));
  })
);

router.get(
  '/:tour_id/products',
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await getSuggestedProducts(parseInt(tour_id, 10)));
  })
);

router.post(
  '/:tour_id/products',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    // eslint-disable-next-line camelcase
    const { product_id } = req.body;
    return res.json(
      await addSuggestedProduct(parseInt(tour_id, 10), product_id)
    );
  })
);

router.delete(
  '/:tour_id/products/:product_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id, product_id } = req.params;
    // eslint-disable-next-line camelcase
    return res.json(
      await deleteSuggestedProduct(
        parseInt(tour_id, 10),
        parseInt(product_id, 10)
      )
    );
  })
);

module.exports = router;
