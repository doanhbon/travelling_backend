const express = require('express');
const asyncHandler = require('express-async-handler');
const {
  updateInforUser,
  updateAvatar,
  getInforUser,
  getBills,
  createBill,
  getBill,
} = require('../services/user');
const { authenticationMiddleware, checkSameUser } = require('../middlewares');
const { uploadImg } = require('../utils/upload');

const router = express.Router();

router.get(
  '/:username',
  authenticationMiddleware,
  checkSameUser,
  asyncHandler(async (req, res) => {
    const { username } = req.params;
    return res.json(await getInforUser(username));
  })
);

router.post(
  '/:username',
  authenticationMiddleware,
  checkSameUser,
  asyncHandler(async (req, res) => {
    const { username } = req.params;
    return res.json(await updateInforUser(req.body, username));
  })
);

router.post(
  '/:username/avatar',
  authenticationMiddleware,
  checkSameUser,
  uploadImg.single('avatar'),
  asyncHandler(async (req, res) => {
    const { username } = req.params;
    return res.json(await updateAvatar(req.file, username));
  })
);

router.get(
  '/:username/bills',
  authenticationMiddleware,
  checkSameUser,
  asyncHandler(async (req, res) => {
    const { id } = req.user;
    return res.json(await getBills(id));
  })
);

router.post(
  '/:username/bills',
  authenticationMiddleware,
  checkSameUser,
  asyncHandler(async (req, res) => {
    const { id } = req.user;
    req.body.user_id = id;
    return res.json(await createBill(req.body));
  })
);

router.get(
  '/:username/bills/:bill_id',
  authenticationMiddleware,
  checkSameUser,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { bill_id } = req.params;
    const { id } = req.user;
    return res.json(await getBill(id, parseInt(bill_id, 10)));
  })
);

module.exports = router;
