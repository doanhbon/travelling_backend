const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkAdmin } = require('../middlewares');
const { signUp } = require('../services/authentication');

const router = express.Router();

router.post(
  '/',
  authenticationMiddleware,
  checkAdmin,
  asyncHandler(async (req, res) => res.json(await signUp(req.body, 'admin')))
);

module.exports = router;
