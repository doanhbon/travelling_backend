const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkEmployee } = require('../middlewares');
const {
  createPlant,
  getAllPlants,
  deletePlant,
  updatePlant,
} = require('../services/plant');

const router = express.Router();

router.post(
  '/:tour_id/plants/',
  authenticationMiddleware,
  checkEmployee,
  // eslint-disable-next-line prettier/prettier
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await createPlant(req.body, tour_id));
  })
);

router.get('/:tour_id/plants/', async (req, res) => {
  // eslint-disable-next-line camelcase
  const { tour_id } = req.params;
  return res.json(await getAllPlants(tour_id));
});

router.delete(
  '/:tour_id/plants/:plant_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id, plant_id } = req.params;
    return res.json(
      await deletePlant(parseInt(tour_id, 10), parseInt(plant_id, 10))
    );
  })
);

router.patch(
  '/:tour_id/plants/:plant_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id, plant_id } = req.params;
    return res.json(await updatePlant(tour_id, plant_id, req.body));
  })
);

module.exports = router;
