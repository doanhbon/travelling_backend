const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkAdmin } = require('../middlewares');
const { signUp } = require('../services/authentication');
const { deleteEmployee, getAllEmployees } = require('../services/employee');

const router = express.Router();

router.post(
  '/',
  authenticationMiddleware,
  checkAdmin,
  asyncHandler(async (req, res) => res.json(await signUp(req.body, 'employee')))
);

router.delete(
  '/',
  authenticationMiddleware,
  checkAdmin,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { employee_id } = req.query;
    res.json(await deleteEmployee(parseInt(employee_id, 10)));
  })
);

router.get(
  '/',
  authenticationMiddleware,
  checkAdmin,
  asyncHandler(async (req, res) => res.json(await getAllEmployees()))
);

module.exports = router;
