const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkEmployee } = require('../middlewares');
const {
  createPlace,
  getAllPlaces,
  deletePlace,
  updatePlace,
  getPlace,
} = require('../services/place');
const { uploadImg } = require('../utils/upload');

const router = express.Router();

router.post(
  '/',
  authenticationMiddleware,
  checkEmployee,
  uploadImg.single('image'),
  // eslint-disable-next-line prettier/prettier
  asyncHandler(async (req, res) => res.json(await createPlace(req.body, req.file)))
);

router.get('/', async (req, res) => res.json(await getAllPlaces()));

router.delete(
  '/:place_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { place_id } = req.params;
    return res.json(await deletePlace(parseInt(place_id, 10)));
  })
);

router.patch(
  '/:place_id',
  authenticationMiddleware,
  checkEmployee,
  uploadImg.single('image'),
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { place_id } = req.params;
    return res.json(await updatePlace(place_id, req.body, req.file));
  })
);

router.get(
  '/:place_id',
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { place_id } = req.params;
    return res.json(await getPlace(parseInt(place_id, 10)));
  })
);

module.exports = router;
