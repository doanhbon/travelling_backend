const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkEmployee } = require('../middlewares');
const {
  createTrip,
  getAllTrips,
  deleteTrip,
  updateTrip,
  getTrip,
  getAllActiveTrips,
} = require('../services/trip');

const router = express.Router();

router.post(
  '/:tour_id/trips/',
  authenticationMiddleware,
  checkEmployee,
  // eslint-disable-next-line prettier/prettier
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await createTrip(req.body, tour_id));
  })
);

router.get('/:tour_id/trips/', async (req, res) => {
  // eslint-disable-next-line camelcase
  const { tour_id } = req.params;
  return res.json(await getAllTrips(tour_id));
});

router.get('/:tour_id/trips/available', async (req, res) => {
  // eslint-disable-next-line camelcase
  const { tour_id } = req.params;
  return res.json(await getAllActiveTrips(tour_id));
});

router.delete(
  '/:tour_id/trips/:trip_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id, trip_id } = req.params;
    return res.json(
      await deleteTrip(parseInt(tour_id, 10), parseInt(trip_id, 10))
    );
  })
);

router.patch(
  '/:tour_id/trips/:trip_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id, trip_id } = req.params;
    return res.json(await updateTrip(tour_id, trip_id, req.body));
  })
);

router.get(
  '/:tour_id/trips/:trip_id',
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { trip_id } = req.params;
    return res.json(await getTrip(trip_id));
  })
);

module.exports = router;
