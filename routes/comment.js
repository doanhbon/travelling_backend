const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware } = require('../middlewares');
const {
  createComment,
  getComments,
  deleteComment,
} = require('../services/comment');

const router = express.Router();

router.post(
  '/:tour_id/comments/',
  authenticationMiddleware,
  asyncHandler(async (req, res) => {
    const { id } = req.user;
    // eslint-disable-next-line camelcase
    const { tour_id } = req.params;
    return res.json(await createComment(req.body, tour_id, id));
  })
);

router.get('/:tour_id/comments/', async (req, res) => {
  // eslint-disable-next-line camelcase
  const { tour_id } = req.params;
  const { limit = 10, offset = 0 } = req.query;
  return res.json(
    await getComments(tour_id, parseInt(limit, 10), parseInt(offset, 10))
  );
});

router.delete(
  '/:tour_id/comments/:comment_id',
  authenticationMiddleware,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { tour_id, comment_id } = req.params;
    const { id, is_employee: isEmployee } = req.user;
    return res.json(
      await deleteComment(
        parseInt(tour_id, 10),
        parseInt(comment_id, 10),
        id,
        isEmployee
      )
    );
  })
);

module.exports = router;
