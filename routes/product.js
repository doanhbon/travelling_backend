const express = require('express');
const asyncHandler = require('express-async-handler');
const { authenticationMiddleware, checkEmployee } = require('../middlewares');
const {
  createProduct,
  getAllProducts,
  deleteProduct,
  updateProduct,
  getProduct,
} = require('../services/product');
const { uploadImg } = require('../utils/upload');

const router = express.Router();

router.post(
  '/',
  authenticationMiddleware,
  checkEmployee,
  uploadImg.single('image'),
  // eslint-disable-next-line prettier/prettier
  asyncHandler(async (req, res) => res.json(await createProduct(req.body, req.file)))
);

router.get('/', async (req, res) =>
  res.json(await getAllProducts({ ...req.query }))
);

router.delete(
  '/:product_id',
  authenticationMiddleware,
  checkEmployee,
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { product_id } = req.params;
    return res.json(await deleteProduct(product_id));
  })
);

router.patch(
  '/:product_id',
  authenticationMiddleware,
  checkEmployee,
  uploadImg.single('image'),
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { product_id } = req.params;
    return res.json(await updateProduct(product_id, req.body, req.file));
  })
);

router.get(
  '/:product_id',
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { product_id } = req.params;
    return res.json(await getProduct(parseInt(product_id, 10)));
  })
);

module.exports = router;
