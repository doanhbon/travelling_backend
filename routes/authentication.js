const express = require('express');
const asyncHandler = require('express-async-handler');
const {
  signUp,
  verifyUser,
  signIn,
  sendMailResetPassword,
  resetPassword,
  sendMailActiveUser,
} = require('../services/authentication');

const router = express.Router();

router.post(
  '/signup',
  asyncHandler(async (req, res) => res.json(await signUp(req.body)))
);

router.post(
  '/verify',
  asyncHandler(async (req, res) => {
    const { token } = req.body;
    return res.json(await verifyUser(token));
  })
);

router.post(
  '/signin',
  asyncHandler(async (req, res) => {
    const { username, password } = req.body;
    return res.json(await signIn(username, password));
  })
);

router.post(
  '/sendmail_reset_password',
  asyncHandler(async (req, res) => {
    const { email } = req.body;
    const result = await sendMailResetPassword(email);
    return res.json(result);
  })
);

router.post(
  '/reset_password',
  asyncHandler(async (req, res) => {
    // eslint-disable-next-line camelcase
    const { token, password, confirm_password } = req.body;
    return res.json(await resetPassword(token, password, confirm_password));
  })
);

router.post(
  '/sendmail_active_user',
  asyncHandler(async (req, res) => {
    const { email } = req.body;
    const result = await sendMailActiveUser(email);
    return res.json(result);
  })
);

module.exports = router;
