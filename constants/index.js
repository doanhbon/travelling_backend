const EMAIL_TEMPLATES = {
  CONFIRM_EMAIL: 'confirmEmailTemplate',
  RESET_PASSWORD_EMAIL: 'resetPasswordEmailTemplate',
};

// eslint-disable-next-line prettier/prettier
const {
  SECRET_KEY,
  REGION,
  PUBLIC_BUCKET,
  RESOURCE_ACCESS_ORIGIN,
} = process.env;

const CUSTOM_ERROR_CODE = {
  SERVER_001: 'Lỗi trong quá trình upload file',

  ADMIN_001: 'Người dùng này không phải nhân viên',

  AUTH_001: 'Người dùng chưa đăng nhập',
  AUTH_002: 'Token không hợp lệ hoặc đã hết hạn',
  AUTH_003: 'User không có quyền thực hiện hành động này',

  USER_001: 'Tài khoản này đã xác thực',
  USER_002: 'Không tìm thấy token xác thực',
  USER_003: 'Token này đã hết hạn',
  USER_004: 'Username này đã tồn tại',
  USER_005: 'Email này đã được sử dụng',
  USER_006: 'Username hoặc mật khẩu không chính xác',
  USER_007: 'Email này chưa được đăng ký',
  USER_008: 'Số điện thoại này đã được sử dụng bởi tài khoản khác',
  USER_010: 'User này chưa được xác thực',
  USER_011: 'Không tìm thấy người dùng',

  PROD_001: 'Tên sản phẩm đã tồn tại',
  PROD_002: 'Không tìm thấy sản phẩm',
  PROD_003: 'Sản phẩm không đủ số lượng',

  PLACE_001: 'Tên địa điểm đã tồn tại',
  PLACE_002: 'Không tìm thấy địa điểm',

  TOUR_001: 'Tên tour đã tồn tại',
  TOUR_002: 'Không tìm thấy tour',

  PLANT_001: 'Kế hoạch không tồn tại',

  TRIP_001: 'Chuyển đi không tồn tại',
  TRIP_002: 'Số vé còn lại không đủ cho bạn',

  DATA_001: 'Email không hợp lệ',
  DATA_002: 'Số điện thoại không hợp lệ',
  DATA_003: 'Password không hợp lệ',
  DATA_004: 'Username không đúng format',
  DATA_005: 'Xác nhận mật khẩu không chính xác',
  DATA_006: 'Mật khẩu cũ không chính xác',
  DATA_007: 'File vừa gửi không phải là hình ảnh',
  DATA_008: 'Url không hợp lệ',
  DATA_009: 'Tên không hợp lệ',
  DATA_010: 'Giá cả phải lớn hơn 0',
  DATA_011: 'Số lượng phải lớn hơn 0',
  DATA_012: 'Địa chỉ không hợp lệ',
  DATA_013: 'Mô tả không hợp lệ',
  DATA_014: 'Giảm giá phải lớn hơn hoặc bằng 0% và bé hơn hoặc bằng 100%',
  DATA_015: 'Số ngày phải lớn hơn hoặc bằng 1',
  DATA_016: 'Số thứ tự ngày phải lớn hơn hoặc bằng 1',
  DATA_017: 'index phải lớn hơn hoặc bằng 1',
  DATA_018: 'Ngày khởi hành phải lớn hơn ngày hiện tại',
  DATA_019: 'Không tìm thấy hình ảnh',
  DATA_020: 'Điểm đánh giá phải từ 0 đến 5',
};

module.exports = {
  EMAIL_TEMPLATES,
  SECRET_KEY,
  CUSTOM_ERROR_CODE,
  REGION,
  PUBLIC_BUCKET,
  RESOURCE_ACCESS_ORIGIN,
};
