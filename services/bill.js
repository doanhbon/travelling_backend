const db = require('../models');

// eslint-disable-next-line no-return-await
const getAllBills = async () =>
  await db.HoaDon.findAll({
  include: [
    { model: db.TourKhoiHanh, include: db.Tour },
    { model: db.User },
    { model: db.ChiTietHoaDon, include: db.SanPham },
  ],
});

const changeBillStatus = async (billId, daThanhToan) => {
  const bill = await db.HoaDon.findByPk(billId);
  bill.da_thanh_toan = daThanhToan;
  const transaction = await db.sequelize.transaction();
  try {
    await bill.save({ transaction });
    await transaction.commit();
    return bill;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

module.exports = {
  getAllBills,
  changeBillStatus,
};
