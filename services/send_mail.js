const AWS = require('aws-sdk');
const { REGION } = require('../constants');

const sendMail = async (destination, templateData, template) => {
  AWS.config.update({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    region: REGION,
  });

  const params = {
    Destination: {
      /* required */ CcAddresses: [],
      ToAddresses: [destination],
    },
    Source: 'atravelingeverywhere@gmail.com' /* required */,
    TemplateData: JSON.stringify(templateData),
    Template: template,
    ReplyToAddresses: [
      /* more items */
    ],
  };
  try {
    const data = await new AWS.SES({ apiVersion: '2010-12-01' })
      .sendTemplatedEmail(params)
      .promise();
    return data;
  } catch (error) {
    if (error) {
      console.log('error = ', error);
    }
    throw error;
  }
};

module.exports = sendMail;
