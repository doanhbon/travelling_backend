const { Op } = require('sequelize');
const {
  CUSTOM_ERROR_CODE,
  RESOURCE_ACCESS_ORIGIN,
  PUBLIC_BUCKET,
} = require('../constants');
const db = require('../models');
const CustomError = require('../utils/custom_error');
const { InputModifyTourDto } = require('../utils/dto/tour');
const { hashString } = require('../utils/hash');
const { uploadToS3, deleteFileOnS3 } = require('../utils/s3');

const createTour = async (tourInfor, image) => {
  if (!image) {
    throw new CustomError('DATA_019', CUSTOM_ERROR_CODE.DATA_019);
  }
  const tourExisting = await db.Tour.findOne({
    where: { ten_tour: tourInfor.ten_tour },
  });

  if (tourExisting) {
    throw new CustomError('TOUR_001', CUSTOM_ERROR_CODE.TOUR_001);
  }

  const fileParts = image.originalname.split('.');
  const extension = fileParts[fileParts.length - 1];
  const hashedName = hashString(`${image.filename}${Date.now()}`);
  const key = `tours/images/${hashedName}.${extension}`;
  const transaction = await db.sequelize.transaction();
  try {
    const tour = await db.Tour.create(
      {
        ...InputModifyTourDto(tourInfor),
        image_url: `https://${RESOURCE_ACCESS_ORIGIN}/${key}`,
      },
      { transaction }
    );
    await transaction.commit();
    await uploadToS3(PUBLIC_BUCKET, key, image);
    return tour;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const handleQuery = (query) => {
  const queryObject = {};

  if (query.ten_tour) {
    queryObject.ten_tour = {
      [Op.like]: `%${query.ten_tour}%`,
    };
  }
  if (query.id) {
    queryObject.id = parseInt(query.id, 10);
  }
  if (query.so_ngay_di) {
    queryObject.so_ngay_di = parseInt(query.so_ngay_di, 10);
  }
  if (query.gia_ve_cho_tre_em_from || query.gia_ve_cho_tre_em_to) {
    queryObject.gia_ve_cho_tre_em = {};

    if (query.gia_ve_cho_tre_em_from) {
      queryObject.gia_ve_cho_tre_em[Op.gte] = parseFloat(
        query.gia_ve_cho_tre_em_from,
        10
      );
    }

    if (query.gia_ve_cho_tre_em_to) {
      queryObject.gia_ve_cho_tre_em[Op.lte] = parseFloat(
        query.gia_ve_cho_tre_em_to,
        10
      );
    }
  }

  if (query.gia_ve_cho_nguoi_lon_from || query.gia_ve_cho_nguoi_lon_to) {
    queryObject.gia_ve_cho_nguoi_lon = {};

    if (query.gia_ve_cho_nguoi_lon_from) {
      queryObject.gia_ve_cho_nguoi_lon[Op.gte] = parseFloat(
        query.gia_ve_cho_nguoi_lon_from,
        10
      );
    }

    if (query.gia_ve_cho_nguoi_lon_to) {
      queryObject.gia_ve_cho_nguoi_lon[Op.lte] = parseFloat(
        query.gia_ve_cho_nguoi_lon_to,
        10
      );
    }
  }
  if (query.dia_diems) {
    const places = query.dia_diems.split(',');
    queryObject[Op.or] = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const place of places) {
      queryObject[Op.or].push({
        '$KeHoachTours.DiaDiem.ten$': {
          [Op.like]: `%${place}%`,
        },
      });
    }
  }

  return queryObject;
};

// eslint-disable-next-line no-return-await
const getAllTours = async (query) => {
  const tours = await db.Tour.findAll({
    include: [
      { model: db.KeHoachTour, include: db.DiaDiem },
      { model: db.SanPham },
    ],
    where: handleQuery(query),
  });
  return tours;
};

const deleteTour = async (id) => {
  const tour = await db.Tour.findByPk(id);

  if (!tour) {
    throw new CustomError('TOUR_002', CUSTOM_ERROR_CODE.TOUR_002);
  }
  await deleteFileOnS3(
    PUBLIC_BUCKET,
    tour.image_url.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
  );

  // eslint-disable-next-line no-return-await
  return await tour.destroy();
};

const updateTour = async (id, productInfor, image) => {
  const tour = await db.Tour.findOne({
    where: { id },
  });

  if (!tour) {
    throw new CustomError('TOUR_002', CUSTOM_ERROR_CODE.TOUR_002);
  }
  const transaction = await db.sequelize.transaction();

  try {
    let key = null;
    const oldImageUrl = tour.image_url;
    if (image) {
      const fileParts = image.originalname.split('.');
      const extension = fileParts[fileParts.length - 1];
      const hashedName = hashString(`${image.filename}${Date.now()}`);
      key = `tours/images/${hashedName}.${extension}`;
      tour.image_url = `https://${RESOURCE_ACCESS_ORIGIN}/${key}`;
    }

    const modifiedField = InputModifyTourDto(productInfor);
    // eslint-disable-next-line no-restricted-syntax
    for (const field of Object.keys(modifiedField)) {
      tour[field] = modifiedField[field];
    }

    await tour.save({ transaction });
    await transaction.commit();
    if (image) {
      await uploadToS3(PUBLIC_BUCKET, key, image);
      if (oldImageUrl) {
        await deleteFileOnS3(
          PUBLIC_BUCKET,
          oldImageUrl.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
        );
      }
    }
    return tour;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

// eslint-disable-next-line no-return-await
const getTour = async (id) =>
  await db.Tour.findByPk(id, {
  include: [
    { model: db.KeHoachTour, include: db.DiaDiem },
    { model: db.TourKhoiHanh },
    { model: db.SanPham },
  ],
});

const getSuggestedProducts = async (id) => {
  const tour = await db.Tour.findByPk(id, {
    include: [{ model: db.SanPham }],
  });
  return tour.SanPhams;
};

const addSuggestedProduct = async (tourId, productId) => {
  const transaction = await db.sequelize.transaction();

  try {
    const toursp = await db.TourSanPham.create(
      {
        TourId: tourId,
        SanPhamId: productId,
      },
      { transaction }
    );
    await transaction.commit();
    return toursp;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const deleteSuggestedProduct = async (tourId, productId) => {
  const transaction = await db.sequelize.transaction();

  try {
    const toursp = await db.TourSanPham.destroy(
      {
        where: {
          TourId: tourId,
          SanPhamId: productId,
        },
      },
      { transaction }
    );
    await transaction.commit();
    return toursp;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

module.exports = {
  createTour,
  getAllTours,
  deleteTour,
  updateTour,
  getTour,
  getSuggestedProducts,
  addSuggestedProduct,
  deleteSuggestedProduct,
};
