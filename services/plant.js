/* eslint-disable no-return-await */
const { CUSTOM_ERROR_CODE } = require('../constants');
const db = require('../models');
const CustomError = require('../utils/custom_error');
const { InputModifyPlantDto } = require('../utils/dto/plant');

const createPlant = async (plantInfor, tourId) => {
  const transaction = await db.sequelize.transaction();
  try {
    const plant = await db.KeHoachTour.create(
      {
        ...InputModifyPlantDto(plantInfor),
        tour_id: tourId,
      },
      { transaction }
    );
    await transaction.commit();
    return plant;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const getAllPlants = async (tourId) => await db.KeHoachTour.findAll({
    where: { tour_id: tourId },
    include: db.DiaDiem,
  });

// eslint-disable-next-line arrow-body-style
const deletePlant = async (tourId, plantId) => {
  return await db.KeHoachTour.destroy({
    where: {
      tour_id: tourId,
      id: plantId,
    },
  });
};

const updatePlant = async (tourId, id, productInfor) => {
  const plant = await db.KeHoachTour.findOne({
    where: { id },
  });

  if (!plant) {
    throw new CustomError('PLANT_001', CUSTOM_ERROR_CODE.PLANT_001);
  }
  const transaction = await db.sequelize.transaction();

  try {
    const modifiedField = InputModifyPlantDto(productInfor);
    // eslint-disable-next-line no-restricted-syntax
    for (const field of Object.keys(modifiedField)) {
      plant[field] = modifiedField[field];
    }

    await plant.save({ transaction });
    await transaction.commit();
    return plant;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

module.exports = {
  createPlant,
  getAllPlants,
  deletePlant,
  updatePlant,
};
