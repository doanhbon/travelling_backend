/* eslint-disable no-param-reassign */
const { InputModifyUserDto } = require('../utils/dto/user');
const { InputModifyBillDto } = require('../utils/dto/hoadon');
const db = require('../models');
const CustomError = require('../utils/custom_error');

const CONSTANTS = require('../constants');
const { uploadToS3, deleteFileOnS3 } = require('../utils/s3');
const { hashString } = require('../utils/hash');

const { CUSTOM_ERROR_CODE, RESOURCE_ACCESS_ORIGIN, PUBLIC_BUCKET } = CONSTANTS;

const updateInforUser = async (data, username) => {
  const transaction = await db.sequelize.transaction();

  try {
    if (data.so_dien_thoai) {
      const user = await db.User.findOne({
        where: { so_dien_thoai: data.so_dien_thoai },
      });

      if (user && user.username !== username.toLowerCase()) {
        throw new CustomError('USER_008', CUSTOM_ERROR_CODE.USER_008);
      }
    }

    if (data.password) {
      if (data.password !== data.confirm_password) {
        throw new CustomError('DATA_005', CUSTOM_ERROR_CODE.DATA_005);
      }
      const user = await db.User.findOne({
        where: { username: username.toLowerCase() },
      });

      if (user && user.password !== data.old_password) {
        throw new CustomError('DATA_006', CUSTOM_ERROR_CODE.DATA_006);
      }
    }

    const dataUpdated = InputModifyUserDto(data);

    await db.User.update(
      { ...dataUpdated },
      { where: { username: username.toLowerCase() }, transaction }
    );
    await transaction.commit();
    return null;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const updateAvatar = async (file, username) => {
  if (!file) {
    throw new CustomError('DATA_019', CUSTOM_ERROR_CODE.DATA_019);
  }
  const fileParts = file.originalname.split('.');
  const extension = fileParts[fileParts.length - 1];
  const hashedName = hashString(`${file.filename}${Date.now()}`);
  const key = `users/${username.toLowerCase()}/avatar/${hashedName}.${extension}`;
  const transaction = await db.sequelize.transaction();
  try {
    await uploadToS3(PUBLIC_BUCKET, key, file);
    const user = await db.User.findOne({
      where: { username: username.toLowerCase() },
    });
    if (user.avatar_url) {
      await deleteFileOnS3(
        PUBLIC_BUCKET,
        user.avatar_url.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
      );
    }
    user.avatar_url = `https://${RESOURCE_ACCESS_ORIGIN}/${key}`;
    await user.save({ transaction });
    await transaction.commit();
    return null;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const getInforUser = async (username) => {
  const user = await db.User.findOne({
    where: { username: username.toLowerCase() },
    attributes: {
      exclude: ['is_verified', 'password'],
    },
  });

  return user;
};

const getBills = async (userId) => {
  const bills = await db.HoaDon.findAll({
    where: { user_id: userId },
    include: [
      { model: db.TourKhoiHanh, include: db.Tour },
      { model: db.ChiTietHoaDon, include: db.SanPham },
    ],
  });

  return bills;
};

const getBill = async (userId, billId) => {
  const bills = await db.HoaDon.findOne({
    where: { user_id: userId, id: billId },
    include: [
      { model: db.TourKhoiHanh, include: db.Tour },
      { model: db.ChiTietHoaDon, include: db.SanPham },
    ],
  });

  return bills;
};

const createBill = async (data) => {
  const newBillData = InputModifyBillDto(data);

  const trip = await db.TourKhoiHanh.findByPk(newBillData.tour_khoi_hanh_id, {
    include: [{ model: db.Tour }],
  });

  if (!trip) {
    throw new CustomError('TRIP_001', CUSTOM_ERROR_CODE.TRIP_001);
  }

  if (trip.so_ve - (data.so_nguoi_lon + data.so_tre_em) < 0) {
    throw new CustomError('TRIP_002', CUSTOM_ERROR_CODE.TRIP_002);
  }
  // eslint-disable-next-line max-len
  newBillData.gia_ve_nguoi_lon =
    trip.Tour.gia_ve_cho_nguoi_lon -
    trip.Tour.gia_ve_cho_nguoi_lon * trip.Tour.giam_gia;
  // eslint-disable-next-line max-len
  newBillData.gia_ve_tre_em =
    trip.Tour.gia_ve_cho_tre_em -
    trip.Tour.gia_ve_cho_tre_em * trip.Tour.giam_gia;
  const products = await db.SanPham.findAll({
    where: {
      id: newBillData.list_products.map((item) => item.id),
    },
  });
  if (!products) {
    throw new CustomError('PROD_002', CUSTOM_ERROR_CODE.PROD_002);
  }
  const transaction = await db.sequelize.transaction();
  try {
    const newBill = await db.HoaDon.create(
      {
        ...newBillData,
      },
      { transaction }
    );
    for (const product of products) {
      const total_buy = newBillData.list_products
        .filter((item) => item.id == product.id)
        .map((item) => item.so_luong)
        .reduce((total, so_luong) => total + so_luong);
      if (product.so_luong - total_buy < 0) {
        throw new CustomError('PROD_003', CUSTOM_ERROR_CODE.PROD_003);
      }

      product.so_luong -= total_buy;
      await product.save({ transaction });

      await db.ChiTietHoaDon.create(
        {
          sanpham_id: product.id,
          hoadon_id: newBill.id,
          gia_mua: product.gia_mua,
          gia_ban: product.gia_ban,
          so_luong: total_buy,
        },
        { transaction }
      );
    }

    await transaction.commit();
    return newBill.reload({
      include: [
        { model: db.TourKhoiHanh, include: db.Tour },
        { model: db.ChiTietHoaDon, include: db.SanPham },
      ],
    });
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

module.exports = {
  updateInforUser,
  updateAvatar,
  getInforUser,
  getBills,
  createBill,
  getBill,
};
