const {
  CUSTOM_ERROR_CODE,
  RESOURCE_ACCESS_ORIGIN,
  PUBLIC_BUCKET,
} = require('../constants');
const db = require('../models');
const CustomError = require('../utils/custom_error');
const { deleteFileOnS3 } = require('../utils/s3');

const deleteEmployee = async (id) => {
  const employee = await db.User.findByPk(id);

  if (
    !employee ||
    employee.is_admin === true ||
    employee.is_employee === false
  ) {
    throw new CustomError('USER_011', CUSTOM_ERROR_CODE.USER_011);
  }
  await deleteFileOnS3(
    PUBLIC_BUCKET,
    employee.avatar_url.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
  );

  // eslint-disable-next-line no-return-await
  return await employee.destroy();
};

const getAllEmployees = async () => {
  const employees = await db.User.findAll({
    where: { is_admin: false, is_employee: true },
  });
  return employees;
};

module.exports = {
  deleteEmployee,
  getAllEmployees,
};
