const {
  CUSTOM_ERROR_CODE,
  RESOURCE_ACCESS_ORIGIN,
  PUBLIC_BUCKET,
} = require('../constants');
const db = require('../models');
const CustomError = require('../utils/custom_error');
const { InputModifyPlaceDto } = require('../utils/dto/place');
const { hashString } = require('../utils/hash');
const { uploadToS3, deleteFileOnS3 } = require('../utils/s3');

const createPlace = async (placeInfor, image) => {
  if (!image) {
    throw new CustomError('DATA_019', CUSTOM_ERROR_CODE.DATA_019);
  }
  const placeExisting = await db.DiaDiem.findOne({
    where: { ten: placeInfor.ten },
  });

  if (placeExisting) {
    throw new CustomError('PLACE_001', CUSTOM_ERROR_CODE.PLACE_001);
  }

  const fileParts = image.originalname.split('.');
  const extension = fileParts[fileParts.length - 1];
  const hashedName = hashString(`${image.filename}${Date.now()}`);
  const key = `places/images/${hashedName}.${extension}`;
  const transaction = await db.sequelize.transaction();
  try {
    const place = await db.DiaDiem.create(
      {
        ...InputModifyPlaceDto(placeInfor),
        image_url: `https://${RESOURCE_ACCESS_ORIGIN}/${key}`,
      },
      { transaction }
    );
    await transaction.commit();
    await uploadToS3(PUBLIC_BUCKET, key, image);
    return place;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

// eslint-disable-next-line no-return-await
const getAllPlaces = async () => await db.DiaDiem.findAll();

const deletePlace = async (id) => {
  const place = await db.DiaDiem.findByPk(id);

  if (!place) {
    throw new CustomError('PLACE_002', CUSTOM_ERROR_CODE.PLACE_002);
  }
  await deleteFileOnS3(
    PUBLIC_BUCKET,
    place.image_url.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
  );

  // eslint-disable-next-line no-return-await
  return await place.destroy();
};

const updatePlace = async (id, productInfor, image) => {
  const place = await db.DiaDiem.findOne({
    where: { id },
  });

  if (!place) {
    throw new CustomError('PLACE_002', CUSTOM_ERROR_CODE.PLACE_002);
  }
  const transaction = await db.sequelize.transaction();

  try {
    let key = null;
    const oldImageUrl = place.image_url;
    if (image) {
      const fileParts = image.originalname.split('.');
      const extension = fileParts[fileParts.length - 1];
      const hashedName = hashString(`${image.filename}${Date.now()}`);
      key = `places/images/${hashedName}.${extension}`;
      place.image_url = `https://${RESOURCE_ACCESS_ORIGIN}/${key}`;
    }

    const modifiedField = InputModifyPlaceDto(productInfor);
    // eslint-disable-next-line no-restricted-syntax
    for (const field of Object.keys(modifiedField)) {
      place[field] = modifiedField[field];
    }

    await place.save({ transaction });
    await transaction.commit();
    if (image) {
      await uploadToS3(PUBLIC_BUCKET, key, image);
      if (oldImageUrl) {
        await deleteFileOnS3(
          PUBLIC_BUCKET,
          oldImageUrl.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
        );
      }
    }
    return place;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

// eslint-disable-next-line no-return-await
const getPlace = async (id) => await db.DiaDiem.findByPk(id);

module.exports = {
  createPlace,
  getAllPlaces,
  deletePlace,
  updatePlace,
  getPlace,
};
