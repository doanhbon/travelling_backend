const { Op } = require('sequelize');
const {
  CUSTOM_ERROR_CODE,
  RESOURCE_ACCESS_ORIGIN,
  PUBLIC_BUCKET,
} = require('../constants');
const db = require('../models');
const CustomError = require('../utils/custom_error');
const { InputModifyProductDto } = require('../utils/dto/product');
const { hashString } = require('../utils/hash');
const { uploadToS3, deleteFileOnS3 } = require('../utils/s3');

const createProduct = async (productInfor, image) => {
  if (!image) {
    throw new CustomError('DATA_019', CUSTOM_ERROR_CODE.DATA_019);
  }
  const productExisting = await db.SanPham.findOne({
    where: { ten_san_pham: productInfor.ten_san_pham },
  });

  if (productExisting) {
    throw new CustomError('PROD_001', CUSTOM_ERROR_CODE.PROD_001);
  }

  const fileParts = image.originalname.split('.');
  const extension = fileParts[fileParts.length - 1];
  const hashedName = hashString(`${image.filename}${Date.now()}`);
  const key = `products/images/${hashedName}.${extension}`;
  const transaction = await db.sequelize.transaction();
  try {
    const product = await db.SanPham.create(
      {
        ...InputModifyProductDto(productInfor),
        image_url: `https://${RESOURCE_ACCESS_ORIGIN}/${key}`,
      },
      { transaction }
    );
    await transaction.commit();
    await uploadToS3(PUBLIC_BUCKET, key, image);
    return product;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const handleQuery = (query) => {
  const queryObject = {};

  if (query.ten_san_pham) {
    queryObject.ten_san_pham = {
      [Op.like]: `%${query.ten_san_pham}%`,
    };
  }
  if (query.id) {
    queryObject.id = parseInt(query.id, 10);
  }

  if (query.gia_ban_from || query.gia_ban_to) {
    queryObject.gia_ban = {};

    if (query.gia_ban_from) {
      queryObject.gia_ban[Op.gte] = parseFloat(query.gia_ban_from, 10);
    }

    if (query.gia_ban_to) {
      queryObject.gia_ban[Op.lte] = parseFloat(query.gia_ban_to, 10);
    }
  }

  return queryObject;
};

// eslint-disable-next-line no-return-await
const getAllProducts = async (query) => {
  const products = await db.SanPham.findAll({
    where: handleQuery(query),
  });
  return products;
};

// eslint-disable-next-line arrow-body-style
const deleteProduct = async (id) => {
  const product = await db.SanPham.findByPk(id);

  if (!product) {
    throw new CustomError('PROD_002', CUSTOM_ERROR_CODE.PROD_002);
  }
  await deleteFileOnS3(
    PUBLIC_BUCKET,
    product.image_url.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
  );

  // eslint-disable-next-line no-return-await
  return await product.destroy();
};

const updateProduct = async (id, productInfor, image) => {
  const product = await db.SanPham.findOne({
    where: { id },
  });

  if (!product) {
    throw new CustomError('PROD_002', CUSTOM_ERROR_CODE.PROD_002);
  }
  const transaction = await db.sequelize.transaction();

  try {
    let key = null;
    const oldImageUrl = product.image_url;

    if (image) {
      const fileParts = image.originalname.split('.');
      const extension = fileParts[fileParts.length - 1];
      const hashedName = hashString(`${image.filename}${Date.now()}`);
      key = `products/images/${hashedName}.${extension}`;
      product.image_url = `https://${RESOURCE_ACCESS_ORIGIN}/${key}`;
    }

    const modifiedField = InputModifyProductDto(productInfor);
    // eslint-disable-next-line no-restricted-syntax
    for (const field of Object.keys(modifiedField)) {
      product[field] = modifiedField[field];
    }

    await product.save({ transaction });
    await transaction.commit();
    if (image) {
      await uploadToS3(PUBLIC_BUCKET, key, image);
      if (oldImageUrl) {
        await deleteFileOnS3(
          PUBLIC_BUCKET,
          oldImageUrl.split(`https://${RESOURCE_ACCESS_ORIGIN}/`)[1]
        );
      }
    }
    return product;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

// eslint-disable-next-line no-return-await
const getProduct = async (id) => await db.SanPham.findByPk(id);

module.exports = {
  createProduct,
  getAllProducts,
  deleteProduct,
  updateProduct,
  getProduct,
};
