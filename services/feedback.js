/* eslint-disable no-return-await */
const db = require('../models');

const createOrUpdateFeedback = async (data, tourId, userId) => {
  const existingFeedback = await db.Feedback.findOne({
    where: {
      tour_id: tourId,
      user_id: userId,
    },
  });
  const transaction = await db.sequelize.transaction();

  try {
    if (existingFeedback) {
      existingFeedback.score = data.score || existingFeedback.score;
      existingFeedback.mota = data.mota || existingFeedback.mota;
      await existingFeedback.save({ transaction });
      await transaction.commit();
      return existingFeedback;
    }
    const feedback = await db.Feedback.create(
      {
        score: data.score,
        mota: data.mota,
        tour_id: tourId,
        user_id: userId,
      },
      { transaction }
    );
    await transaction.commit();
    return feedback;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const getFeedbacks = async (tourId, limit, offset) => {
  const res = await db.Feedback.findAll({
    where: { tour_id: tourId, is_valid: true },
    include: [
      {
        model: db.User,
        attributes: {
          exclude: [
            'email',
            'so_dien_thoai',
            'ngay_sinh',
            'password',
            'is_verified',
            'is_admin',
            'is_employee',
            'createdAt',
            'updatedAt',
          ],
        },
      },
    ],
    order: [['id', 'DESC']],
  });
  return {
    count: res.length,
    data: res.slice(offset, offset + limit),
  };
};

// eslint-disable-next-line arrow-body-style
const deleteFeedback = async (tourId, feedbackId, userId) => {
  return await db.Feedback.destroy({
    where: {
      tour_id: tourId,
      id: feedbackId,
      user_id: userId,
    },
  });
};

module.exports = {
  createOrUpdateFeedback,
  getFeedbacks,
  deleteFeedback,
};
