/* eslint-disable no-return-await */
const { Op } = require('sequelize');

const { CUSTOM_ERROR_CODE } = require('../constants');
const db = require('../models');
const CustomError = require('../utils/custom_error');
const { InputModifyTripDto } = require('../utils/dto/trip');

const createTrip = async (plantInfor, tourId) => {
  const transaction = await db.sequelize.transaction();
  try {
    const trip = await db.TourKhoiHanh.create(
      {
        ...InputModifyTripDto(plantInfor),
        so_ve_con_lai: plantInfor.so_ve,
        tour_id: tourId,
      },
      { transaction }
    );
    await transaction.commit();
    return trip;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const getAllTrips = async (tourId) =>
  await db.TourKhoiHanh.findAll({
  where: { tour_id: tourId },
  include: [{ model: db.Tour }],
});

const getAllActiveTrips = async (tourId) =>
  await db.TourKhoiHanh.findAll({
  where: {
    tour_id: tourId,
    ngay_xuat_phat: {
      [Op.gte]: new Date().toISOString(),
    },
  },
});

// eslint-disable-next-line arrow-body-style
const deleteTrip = async (tourId, tripId) => {
  return await db.TourKhoiHanh.destroy({
    where: {
      tour_id: tourId,
      id: tripId,
    },
  });
};

const updateTrip = async (tourId, id, tripInfor) => {
  const trip = await db.TourKhoiHanh.findOne({
    where: { id },
  });

  if (!trip) {
    throw new CustomError('TRIP_001', CUSTOM_ERROR_CODE.TRIP_001);
  }
  const transaction = await db.sequelize.transaction();

  try {
    const modifiedField = InputModifyTripDto(tripInfor);
    if (modifiedField.so_ve) {
      trip.so_ve_con_lai += tripInfor.so_ve - trip.so_ve;
    }

    // eslint-disable-next-line no-restricted-syntax
    for (const field of Object.keys(modifiedField)) {
      trip[field] = modifiedField[field];
    }

    await trip.save({ transaction });
    await transaction.commit();
    return trip;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const getTrip = async (id) =>
  await db.TourKhoiHanh.findByPk(id, {
  include: [{ model: db.Tour }],
});

module.exports = {
  createTrip,
  getAllTrips,
  deleteTrip,
  updateTrip,
  getTrip,
  getAllActiveTrips,
};
