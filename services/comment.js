/* eslint-disable no-return-await */
const db = require('../models');

const createComment = async (data, tourId, userId) => {
  const transaction = await db.sequelize.transaction();

  try {
    const comment = await db.Comment.create(
      {
        noi_dung: data.noi_dung ? data.noi_dung.trim() : null,
        parent_id: data.parent_id,
        tour_id: tourId,
        user_id: userId,
      },
      { transaction }
    );
    await transaction.commit();
    return comment;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const getComments = async (tourId, limit, offset) => {
  const res = await db.Comment.findAll({
    where: { tour_id: tourId, parent_id: null },
    include: [
      {
        model: db.User,
        attributes: {
          exclude: [
            'email',
            'so_dien_thoai',
            'ngay_sinh',
            'password',
            'is_verified',
            'is_admin',
            'is_employee',
            'createdAt',
            'updatedAt',
          ],
        },
      },
      {
        model: db.Comment,
      },
    ],
    order: [['id', 'DESC']],
  });
  return {
    count: res.length,
    data: res.slice(offset, offset + limit),
  };
};

// eslint-disable-next-line arrow-body-style
const deleteComment = async (tourId, commentId, userId, isEmployee) => {
  if (isEmployee) {
    return await db.Comment.destroy({
      where: {
        tour_id: tourId,
        id: commentId,
      },
    });
  }
  return await db.Comment.destroy({
    where: {
      tour_id: tourId,
      id: commentId,
      user_id: userId,
    },
  });
};

module.exports = {
  createComment,
  getComments,
  deleteComment,
};
