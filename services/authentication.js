const { Op } = require('sequelize');
const jwt = require('jsonwebtoken');

const {
  generateConfirmToken,
  decodeConfirmToken,
} = require('../utils/generate_keys');
const db = require('../models');
const CustomError = require('../utils/custom_error');

const sendMail = require('./send_mail');

const CONSTANTS = require('../constants');

const { CUSTOM_ERROR_CODE, SECRET_KEY, EMAIL_TEMPLATES } = CONSTANTS;

const signUp = async (dataUser, signUpFor = 'user') => {
  const existingUser = await db.User.findOne({
    where: {
      [Op.or]: [
        { username: dataUser.username.toLowerCase() },
        { email: dataUser.email.toLowerCase() },
      ],
    },
  });

  if (existingUser) {
    if (existingUser.username === dataUser.username.toLowerCase()) {
      throw new CustomError('USER_004', CUSTOM_ERROR_CODE.USER_004);
    }
    throw new CustomError('USER_005', CUSTOM_ERROR_CODE.USER_005);
  }

  const transaction = await db.sequelize.transaction();

  try {
    const newUser = await db.User.create(
      {
        ...dataUser,
        username: dataUser.username.toLowerCase(),
        email: dataUser.email.toLowerCase(),
        is_admin: signUpFor === 'admin',
        is_employee: signUpFor === 'employee' || signUpFor === 'admin',
      },
      { transaction }
    );
    const timeToConfirmEmail = Date.now() + 15 * 60 * 1000;
    const confirmToken = generateConfirmToken(
      JSON.stringify({
        id: newUser.id,
        expiredTime: timeToConfirmEmail,
      })
    );
    await db.UserAuthentication.create(
      {
        user_id: newUser.id,
        confirm_email_token: confirmToken,
      },
      { transaction }
    );
    // eslint-disable-next-line max-len
    const sendMailData = await sendMail(
      dataUser.email,
      {
        data: {
          username: dataUser.username,
          hyperlink: `https://traveling-everywhere.net/verify/${confirmToken}`,
        },
        subject: 'Xác thực tài khoản',
      },
      EMAIL_TEMPLATES.CONFIRM_EMAIL
    );

    if (!sendMailData) {
      await transaction.rollback();
      return null;
    }
    await transaction.commit();
    return newUser;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

// eslint-disable-next-line consistent-return
const verifyUser = async (token) => {
  const dataVerification = JSON.parse(decodeConfirmToken(token));

  if (dataVerification) {
    if (dataVerification.expiredTime < Date.now()) {
      throw new CustomError('USER_003', CUSTOM_ERROR_CODE.USER_003);
    }

    const userAuthentication = await db.UserAuthentication.findOne({
      where: { user_id: dataVerification.id, confirm_email_token: token },
      include: db.User,
    });

    if (userAuthentication) {
      const transaction = await db.sequelize.transaction();

      try {
        if (userAuthentication.User.is_verified) {
          throw new CustomError('USER_001', CUSTOM_ERROR_CODE.USER_001);
        }

        await db.UserAuthentication.update(
          { confirm_email_token: '' },
          {
            where: { user_id: dataVerification.id },
          }
        );
        await db.User.update(
          { is_verified: true },
          { where: { id: dataVerification.id } }
        );
        await transaction.commit();
        return null;
      } catch (error) {
        await transaction.rollback();
        throw error;
      }
    }

    throw new CustomError('USER_002', CUSTOM_ERROR_CODE.USER_002);
  }
};

const signIn = async (username, password) => {
  const user = await db.User.findOne({
    where: {
      [Op.and]: [
        { password },
        {
          [Op.or]: [
            { username: username.toLowerCase() },
            { email: username.toLowerCase() },
          ],
        },
      ],
    },
  });

  if (!user) {
    throw new CustomError('USER_006', CUSTOM_ERROR_CODE.USER_006);
  }

  if (!user.is_verified) {
    throw new CustomError('USER_010', CUSTOM_ERROR_CODE.USER_010);
  }

  const token = jwt.sign(
    {
      is_admin: user.is_admin,
      username: user.username,
      is_employee: user.is_employee,
      email: user.email,
      id: user.id,
    },
    SECRET_KEY,
    { expiresIn: 3600000 } // 1 hour
  );

  return { token, username: user.username };
};

const sendMailResetPassword = async (email) => {
  const transaction = await db.sequelize.transaction();

  try {
    const user = await db.User.findOne({
      where: {
        email: email.toLowerCase(),
      },
    });

    if (!user) {
      throw new CustomError('USER_007', CUSTOM_ERROR_CODE.USER_007);
    }

    const resetPasswordToken = generateConfirmToken(
      JSON.stringify({
        id: user.id,
        expiredTime: Date.now() + 15 * 60 * 1000,
      })
    );

    await db.UserAuthentication.update(
      { reset_password_token: resetPasswordToken },
      {
        where: {
          user_id: user.id,
        },
        transaction,
      }
    );

    const sendMailData = await sendMail(
      email,
      {
        data: {
          username: user.username,
          hyperlink: `https://traveling-everywhere.net/reset/${resetPasswordToken}`,
        },
        subject: 'Đặt lại mật khẩu',
      },
      EMAIL_TEMPLATES.RESET_PASSWORD_EMAIL
    );

    if (!sendMailData) {
      await transaction.rollback();
      return null;
    }
    await transaction.commit();
    return null;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const sendMailActiveUser = async (email) => {
  const user = await db.User.findOne({
    where: {
      email: email.toLowerCase(),
    },
  });

  if (!user) {
    throw new CustomError('USER_007', CUSTOM_ERROR_CODE.USER_007);
  }

  if (user.is_verified) {
    throw new CustomError('USER_001', CUSTOM_ERROR_CODE.USER_001);
  }
  const transaction = await db.sequelize.transaction();

  try {
    const confirmToken = generateConfirmToken(
      JSON.stringify({
        id: user.id,
        expiredTime: Date.now() + 15 * 60 * 1000,
      })
    );

    await db.UserAuthentication.update(
      { confirm_email_token: confirmToken },
      {
        where: {
          user_id: user.id,
        },
        transaction,
      }
    );

    const sendMailData = await sendMail(
      email,
      {
        data: {
          username: user.username,
          hyperlink: `https://traveling-everywhere.net/verify/${confirmToken}`,
        },
        subject: 'Xác thực tài khoản',
      },
      EMAIL_TEMPLATES.CONFIRM_EMAIL
    );

    if (!sendMailData) {
      await transaction.rollback();
      return null;
    }
    await transaction.commit();
    return null;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

// eslint-disable-next-line consistent-return
const resetPassword = async (token, password, confirmPassword) => {
  if (password !== confirmPassword) {
    throw new CustomError('DATA_005', CUSTOM_ERROR_CODE.DATA_005);
  }

  const dataUser = JSON.parse(decodeConfirmToken(token));
  if (dataUser) {
    if (dataUser.expiredTime < Date.now()) {
      throw new CustomError('USER_003', CUSTOM_ERROR_CODE.USER_003);
    }

    const userAuthentication = await db.UserAuthentication.findOne({
      where: { user_id: dataUser.id, reset_password_token: token },
    });

    if (userAuthentication) {
      const transaction = await db.sequelize.transaction();

      try {
        userAuthentication.reset_password_token = '';
        await userAuthentication.save();

        await db.User.update(
          { password },
          { where: { id: dataUser.id }, transaction }
        );
        await transaction.commit();
        return null;
      } catch (error) {
        await transaction.rollback();
        throw error;
      }
    }

    throw new CustomError('USER_002', CUSTOM_ERROR_CODE.USER_002);
  }
};

module.exports = {
  signUp,
  verifyUser,
  signIn,
  sendMailResetPassword,
  resetPassword,
  sendMailActiveUser,
};
