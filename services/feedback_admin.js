/* eslint-disable no-return-await */
const db = require('../models');

const getFeedbacks = async (limit, offset, queryParams) => {
  const res = await db.Feedback.findAll({
    where: { ...queryParams },
    include: [
      {
        model: db.User,
        attributes: {
          exclude: [
            'email',
            'so_dien_thoai',
            'ngay_sinh',
            'password',
            'is_verified',
            'is_admin',
            'is_employee',
            'createdAt',
            'updatedAt',
          ],
        },
      },
    ],
    order: [['id', 'DESC']],
  });
  return {
    count: res.length,
    data: res.slice(offset, offset + limit),
  };
};

// eslint-disable-next-line arrow-body-style
const deleteFeedback = async (feedbackId) => {
  const feedback = await db.Feedback.findByPk(feedbackId);
  const transaction = await db.sequelize.transaction();
  try {
    const tour = await db.Tour.findByPk(feedback.tour_id);
    tour.so_luot_danh_gia -= 1;
    tour.diem_danh_gia_trung_binh =
      (tour.diem_danh_gia_trung_binh * (tour.so_luot_danh_gia + 1) -
        feedback.score) /
      tour.so_luot_danh_gia;
    await tour.save({ transaction });
    const res = await feedback.destroy({ transaction });
    await transaction.commit();
    return res;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

const changeFeedbackStatus = async (feedbackId, isValid) => {
  const feedback = await db.Feedback.findByPk(feedbackId);
  feedback.is_valid = isValid;
  const transaction = await db.sequelize.transaction();
  try {
    await feedback.save({ transaction });
    const tour = await db.Tour.findByPk(feedback.tour_id);
    tour.so_luot_danh_gia += 1;
    tour.diem_danh_gia_trung_binh =
      (tour.diem_danh_gia_trung_binh * (tour.so_luot_danh_gia - 1) +
        feedback.score) /
      tour.so_luot_danh_gia;
    await tour.save({ transaction });
    await transaction.commit();
    return feedback;
  } catch (error) {
    await transaction.rollback();
    throw error;
  }
};

module.exports = {
  getFeedbacks,
  deleteFeedback,
  changeFeedbackStatus,
};
