const { ValidationError } = require('sequelize');

const CustomError = require('./custom_error');

const customResponseData = (data) => {
  let formattedResponseData = null;
  if (data instanceof ValidationError) {
    formattedResponseData = {
      status: 'ERROR',
      message: 'Truy vấn không thành công!',
      errors: [
        {
          code: data.errors[0].original.code,
          message: data.errors[0].original.description,
        },
      ],
    };
  } else if (data instanceof CustomError) {
    formattedResponseData = {
      status: 'ERROR',
      message: 'Truy vấn không thành công!',
      errors: [
        {
          code: data.code,
          message: data.description,
        },
      ],
    };
  } else if (data instanceof Error) {
    formattedResponseData = {
      status: 'ERROR',
      message: 'Truy vấn không thành công!',
      errors: [
        {
          code: 'SERVER_000',
          message: 'Lỗi chưa xác định.',
        },
      ],
    };
  } else {
    formattedResponseData = {
      status: 'SUCCESS',
      message: 'Truy vấn thành công!',
      errors: null,
      data,
    };
  }

  return formattedResponseData;
};

module.exports = {
  customResponseData,
};
