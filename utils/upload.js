const multer = require('multer');
const path = require('path');

const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('./custom_error');

const storage = multer.memoryStorage({
  destination(req, file, callback) {
    callback(null, '');
  },
});

function checkFileImageType(file, cb) {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);
  if (mimetype && extname) {
    return cb(null, true);
  }
  throw new CustomError('DATA_007', CUSTOM_ERROR_CODE);
}

const uploadImg = multer({
  storage,
  limits: { fileSize: 2000000 }, // In bytes: 2000000 bytes = 2 MB
  fileFilter(req, file, cb) {
    checkFileImageType(file, cb);
  },
});

module.exports = {
  uploadImg,
};
