const InputModifyTripDto = (obj) => {
  const result = {
    ...obj,
  };

  const listKeyNeedDelete = [
    'id',
    'tour_id',
    'so_ve_con_lai',
    'createdAt',
    'updateAt',
  ];
  // eslint-disable-next-line no-restricted-syntax
  for (const key of listKeyNeedDelete) {
    if (result[key] !== null && result[key] !== undefined) {
      delete result[key];
    }
  }

  return result;
};

module.exports = {
  InputModifyTripDto,
};
