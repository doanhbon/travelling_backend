const InputModifyUserDto = (obj) => {
  const result = {
    ...obj,
  };

  const listKeyNeedDelete = [
    'is_verified',
    'is_admin',
    'username',
    'email',
    'id',
    'confirm_password',
    'old_password',
    'is_employee',
    'avatar_url',
    'createdAt',
    'updateAt',
  ];
  // eslint-disable-next-line no-restricted-syntax
  for (const key of listKeyNeedDelete) {
    if (result[key] !== null && result[key] !== undefined) {
      delete result[key];
    }
  }

  return result;
};

module.exports = {
  InputModifyUserDto,
};
