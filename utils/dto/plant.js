const InputModifyPlantDto = (obj) => {
  const result = {
    ...obj,
  };

  const listKeyNeedDelete = ['id', 'tour_id', 'createdAt', 'updateAt'];
  // eslint-disable-next-line no-restricted-syntax
  for (const key of listKeyNeedDelete) {
    if (result[key] !== null && result[key] !== undefined) {
      delete result[key];
    }
  }

  return result;
};

module.exports = {
  InputModifyPlantDto,
};
