const InputModifyTourDto = (obj) => {
  const result = {
    ...obj,
  };

  const listKeyNeedDelete = [
    'id',
    'image_url',
    'createdAt',
    'updateAt',
    'so_luot_danh_gia',
    'diem_danh_gia_trung_binh',
  ];
  // eslint-disable-next-line no-restricted-syntax
  for (const key of listKeyNeedDelete) {
    if (result[key] !== null && result[key] !== undefined) {
      delete result[key];
    }
  }

  return result;
};

module.exports = {
  InputModifyTourDto,
};
