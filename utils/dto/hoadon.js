const InputModifyBillDto = (obj) => {
  const result = {
    ...obj,
  };

  const listKeyNeedDelete = [
    'gia_ve_nguoi_lon',
    'gia_ve_tre_em',
    'id',
    'da_thanh_toan',
    'createdAt',
    'updateAt',
  ];
  // eslint-disable-next-line no-restricted-syntax
  for (const key of listKeyNeedDelete) {
    if (result[key] !== null && result[key] !== undefined) {
      delete result[key];
    }
  }

  return result;
};

module.exports = {
  InputModifyBillDto,
};
