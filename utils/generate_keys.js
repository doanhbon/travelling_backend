/* eslint-disable implicit-arrow-linebreak */
const CryptoJS = require('crypto-js');
const CONSTANTS = require('../constants');

const { SECRET_KEY } = CONSTANTS;

const generateConfirmToken = (data) =>
  CryptoJS.AES.encrypt(data, SECRET_KEY).toString();

const decodeConfirmToken = (key) =>
  CryptoJS.AES.decrypt(key, SECRET_KEY).toString(CryptoJS.enc.Utf8);

module.exports = {
  generateConfirmToken,
  decodeConfirmToken,
};
