/* eslint-disable indent */
/* eslint-disable prettier/prettier */
const AWS = require('aws-sdk');
const { CUSTOM_ERROR_CODE } = require('../constants');
const CustomError = require('./custom_error');

const s3 = new AWS.S3({
  accessKeyId: process.env.ACCESS_KEY_ID,
  secretAccessKey: process.env.SECRET_ACCESS_KEY,
});

const uploadToS3 = (bucket, key, file) => new Promise((resolve, reject) => {
    const params = {
      Bucket: bucket,
      Key: key,
      Body: file.buffer,
      ContentType: file.mimetype,
    };
    s3.upload(params, (error, data) => {
      if (error) {
        return reject(error);
      }

      return resolve(data);
    });
  });

const deleteFileOnS3 = (bucket, key) => new Promise((resolve, reject) => {
  const params = {
    Bucket: bucket,
    Key: key,
  };
  s3.deleteObject(params, (error, data) => {
    if (error) {
      return reject(new CustomError('SERVER_001', CUSTOM_ERROR_CODE.SERVER_001));
    }

    return resolve(data);
  });
});

module.exports = {
  uploadToS3,
  deleteFileOnS3,
};
