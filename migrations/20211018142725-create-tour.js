module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Tours', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      ten_tour: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      mo_ta: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      gia_ve_cho_tre_em: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      gia_ve_cho_nguoi_lon: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      so_ngay_di: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      giam_gia: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      image_url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Tours');
  },
};
