module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('DiaDiems', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      ten: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
      },
      dia_chi: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      mo_ta: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      chi_phi_goc: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      gia_khi_qua_dia_diem: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      image_url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('DiaDiems');
  },
};
