module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('ChiTietHoaDons', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      gia_mua: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      gia_ban: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      so_luong: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      sanpham_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'SanPhams', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      hoadon_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'HoaDons', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('ChiTietHoaDons');
  },
};
