module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Comments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      noi_dung: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      tour_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Tours', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      parent_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Comments', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: true,
        onDelete: 'CASCADE',
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Comments');
  },
};
