module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('SanPhams', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      ten_san_pham: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
      },
      mo_ta: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      gia_mua: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      gia_ban: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      so_luong: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
      image_url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('SanPhams');
  },
};
