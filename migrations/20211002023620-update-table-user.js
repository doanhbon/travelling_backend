module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.changeColumn(
        'Users',
        'ho',
        {
          type: Sequelize.DataTypes.STRING,
        },
        { transaction }
      );

      await queryInterface.changeColumn(
        'Users',
        'ten',
        {
          type: Sequelize.DataTypes.STRING,
        },
        { transaction }
      );

      await queryInterface.changeColumn(
        'Users',
        'email',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
          unique: true,
          isEmail: true,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'Users',
        'gioi_tinh',
        {
          type: Sequelize.DataTypes.BOOLEAN,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'Users',
        'so_dien_thoai',
        {
          type: Sequelize.DataTypes.STRING,
          unique: true,
          is: /^[0-9]{10}$/i,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'Users',
        'ngay_sinh',
        {
          type: Sequelize.DataTypes.DATEONLY,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'Users',
        'username',
        {
          type: Sequelize.DataTypes.STRING(32),
          allowNull: false,
          unique: true,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'Users',
        'password',
        {
          type: Sequelize.DataTypes.STRING(64),
          allowNull: false,
          is: /^[0-9a-f]{64}$/i,
        },
        { transaction }
      );
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.changeColumn(
        'Users',
        'ho',
        {
          type: Sequelize.DataTypes.STRING,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        'Users',
        'ten',
        {
          type: Sequelize.DataTypes.STRING,
        },
        { transaction }
      );
      await queryInterface.changeColumn(
        'Users',
        'email',
        {
          type: Sequelize.DataTypes.STRING,
        },
        { transaction }
      );

      await queryInterface.removeColumn('Users', 'gioi_tinh', { transaction });

      await queryInterface.removeColumn('Users', 'so_dien_thoai', {
        transaction,
      });

      await queryInterface.removeColumn('Users', 'ngay_sinh', { transaction });

      await queryInterface.removeColumn('Users', 'username', { transaction });

      await queryInterface.removeColumn('Users', 'password', { transaction });
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
