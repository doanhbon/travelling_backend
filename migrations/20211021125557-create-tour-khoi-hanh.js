module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('TourKhoiHanhs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      so_ve: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      so_ve_con_lai: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      ngay_xuat_phat: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      mo_ta: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      tour_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Tours', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('TourKhoiHanhs');
  },
};
