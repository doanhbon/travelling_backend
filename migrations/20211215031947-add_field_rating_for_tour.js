module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'Tours',
        'so_luot_danh_gia',
        {
          type: Sequelize.INTEGER.UNSIGNED,
          allowNull: false,
          defaultValue: 0,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Tours',
        'diem_danh_gia_trung_binh',
        {
          type: Sequelize.INTEGER.UNSIGNED,
          allowNull: false,
          defaultValue: 0,
        },
        { transaction }
      );
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('Tours', 'so_luot_danh_gia', {
        transaction,
      });
      await queryInterface.removeColumn('Tours', 'diem_danh_gia_trung_binh', {
        transaction,
      });
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
