module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('KeHoachTours', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      mo_ta: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      thu_tu_ngay: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: 'compositeIndexThutu',
      },
      index: {
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: 'compositeIndexThutu',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      tour_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Tours', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      dia_diem_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'DiaDiems', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: true,
        onDelete: 'CASCADE',
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('KeHoachTours');
  },
};
