module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'HoaDons',
        'ho_ten',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'so_dien_thoai',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'dia_chi',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'so_cmnd',
        {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'ngay_sinh',
        {
          type: Sequelize.DataTypes.DATEONLY,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'so_nguoi_lon',
        {
          type: Sequelize.DataTypes.INTEGER,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'so_tre_em',
        {
          type: Sequelize.DataTypes.INTEGER,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'gia_ve_nguoi_lon',
        {
          type: Sequelize.DataTypes.DOUBLE,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'gia_ve_tre_em',
        {
          type: Sequelize.DataTypes.DOUBLE,
          allowNull: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HoaDons',
        'tour_khoi_hanh_id',
        {
          type: Sequelize.DataTypes.INTEGER,
          allowNull: false,
          onDelete: 'CASCADE',
          references: {
            model: 'TourKhoiHanhs', // name of Target model
            key: 'id', // key in Target model that we're referencing
          },
        },
        { transaction }
      );
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('HoaDons', 'ho_ten', { transaction });

      await queryInterface.removeColumn('HoaDons', 'so_dien_thoai', {
        transaction,
      });

      await queryInterface.removeColumn('HoaDons', 'dia_chi', { transaction });

      await queryInterface.removeColumn('HoaDons', 'so_cmnd', { transaction });

      await queryInterface.removeColumn('HoaDons', 'ngay_sinh', {
        transaction,
      });
      await queryInterface.removeColumn('HoaDons', 'so_nguoi_lon', {
        transaction,
      });
      await queryInterface.removeColumn('HoaDons', 'so_tre_em', {
        transaction,
      });
      await queryInterface.removeColumn('HoaDons', 'gia_ve_nguoi_lon', {
        transaction,
      });
      await queryInterface.removeColumn('HoaDons', 'gia_ve_tre_em', {
        transaction,
      });
      await queryInterface.removeColumn('HoaDons', 'tour_khoi_hanh_id', {
        transaction,
      });
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
