module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'UserAuthentications',
        'reset_password_token',
        {
          type: Sequelize.DataTypes.STRING,
        }
      );
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn(
        'UserAuthentications',
        'reset_password_token',
        {
          transaction,
        }
      );
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
