module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('TourSanPham', {
      SanPhamId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'SanPhams', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      TourId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'Tours', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('TourSanPham');
  },
};
