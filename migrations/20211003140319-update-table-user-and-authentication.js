module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn(
        'Users',
        'is_verified',
        {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Users',
        'is_admin',
        {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        { transaction }
      );
      await queryInterface.addColumn('UserAuthentications', 'user_id', {
        type: Sequelize.DataTypes.INTEGER,
        references: {
          model: 'Users', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        allowNull: false,
        onDelete: 'CASCADE',
      });
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Users', 'is_verified', {
        transaction,
      });

      await queryInterface.removeColumn('Users', 'is_admin', { transaction });
      await queryInterface.removeColumn('UserAuthentications', 'user_id', {
        transaction,
      });
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
